# ADM Equations in Spherical Symmetry

Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

Time-stamp: <2015-03-29 21:52:25 (jmiller)>

## Discussion

This program solves the ADM equations in spherical symmetry, allowing
one to solve for the gravitational field, given some matter
content. The precise formulation of the Einstein Equations can be
found in the adm-spherical-symmetry.pdf file included.

## Installation

To run the code, simply navigate to the appropriate folder and run
adm_spherical_symmetry.py through python 2. You need to have the
full scipy stack installed, including numpy, scipy, and
matplotlib. You can choose a matter content by opening up
adm_spherical_symmetry.py and choosing a new variable for
MATTER_CONTENT, which should be a vector-valued function of
radius. It should return an array [rho,j^r,S], where rho, jr, and
S are as defined in the pdf.

Assuming no discontinuities in the lapse, this code can be plugged
directly into a hyperbolic solver for some fluid system and one can
evolve things like gravitational collapse. That's the main goal of
this library.

## Code Structure

We first solve for the quantities associated with a spacelike
hypersurface (constant time slice): the 3-metric and the extrinsic
curvature. Each of these quantities has only one degree of freedom,
the (r,r)-component. For the metric, we call this "a." For the
extrinsic curvature, we call this "krr."

We solve for the hypersurface quantities by treating the ADM
constraint equations as initial value problems, imposing spherical
symmetry and regularization conditions at the origin, and numerically
integrating outward with an adaptive Runge-Kutte integrator.

After we have the hypersurface quantities, we solve for the gauge
quantities, the lapse and the shift. The lapse is determined by a
second-order ordinary differential equation which is posed as a
boundary-value problem. Indeed, naive attempts to treat it as an
initial-value problem fail since it seems to be ill-posed when viewed
in this way.

Therefore, we solve the for lapse using a colocation spectral method
using Legendre polynomials as the orthogonal basis. To find the roots
of the residual, we use a Newton-Krylov linear solver.

The shift is given algebraically as a function of the lapse and the
extrinsic curvature.

### Files
* adm_spherical_symmetry.py --- This is the UI and contains the main function.
* data_structures.py        --- Contains definitions of the arrays used when solving
                                the ADM constraint equations and some helper functions
                                for reconstructing the full four-metric.
* boundary_data.py          --- Contains methods that define what should happen at
                                r=0 and asymptotically at the outer edge of the domain.
* rhs.py                    --- Writes the Einstein constraint equations in the form
                                (d/dr) u = f(u)
                                where u is a vector and f is a vector-valued function.
* matter.py                 --- Defines several matter distributions for testing
* integrator.py             --- Numerically integrates the Einstein constraint equations
                                outward from the origin.
* orthopoly.py              --- Defines the colocation points, integration weights,
                                fast Fourier transforms, and derivative matrices
                                required for a pseudospectral method.
* lapse.py                  --- Pseudospectral elliptic solver for the lapse.