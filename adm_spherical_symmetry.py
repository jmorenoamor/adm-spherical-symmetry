#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 23:31:50 (jmiller)>

This little module solves the ADM equations for gravity in
spherical symmetry. In spherical symmetry, everything can be
determined by gauge conditions and the constraint equations, so that's
what we do.

This program only handles a single space-like hypersurface. This
completely determines the gravity sector, but not the matter
sector. The matter must be determined separately.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # numpy arrays

# this program
import data_structures
import boundary_data
import integrator
import rhs
import matter
import visualization
import lapse
from orthopoly import PseudoSpectralStencil
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------


# Constants
# ----------------------------------------------------------------------
RMIN=0
RMAX=50
R_NPOINTS=50
#MATTER_CONTENT=matter.vacuum
# MATTER_CONTENT=lambda r: matter.uniformly_infalling_gaussian_dust(r,
#                                                                   0.001,
#                                                                   0.5*RMAX,
#                                                                   0.25*RMAX,
#                                                                   0)
#MATTER_CONTENT = lambda r: matter.uniformly_infalling_gaussian_dust(r,0.01, 0,100,0)
#MATTER_CONTENT = lambda r: matter.gaussian_star(r,1./400,5,np.sqrt(2))
MATTER_CONTENT = lambda r: matter.uniformly_infalling_gaussian_dust(r,
                                                                    1./200,5,
                                                                    np.sqrt(2),
                                                                    -1E-1)
#MATTER_CONTENT=lambda r: matter.gaussian_star(r,stress=100,n=0.5)
# ----------------------------------------------------------------------


def main():
    stencil = PseudoSpectralStencil(R_NPOINTS,RMIN,RMAX)
    rcoords = stencil.get_x()
    hypersurface = integrator.solve_hypersurface(rcoords,MATTER_CONTENT)
    alpha = lapse.solve_for_lapse(stencil,hypersurface,MATTER_CONTENT)
    beta = data_structures.get_shift(rcoords,hypersurface[...,K_I],alpha)
    visualization.plot_hypersurface(stencil,hypersurface,alpha,beta,MATTER_CONTENT)
    return

if __name__ == "__main__":
    main()
