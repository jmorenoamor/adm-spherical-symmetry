#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 20:23:20 (jmiller)>

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

Here we define routines to handle boundary conditions.

This spherical symmetry condition, combined with the need that our
functions be regular at the origin, determines a up to a sign, and
determines krr completely.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # numpy arrays
# this program
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------

# The values of gravity quantities at origin:
A_0 = 1 # a(r=0)
K_RR_0 = 0 # krr(r=0)
ALEPH_0 = 0 # aleph(r=0)

def get_initial_hypersurface_vector(sign_a=1):
    """
    Returns the hypersurface vector at r = 0. Spherical coordinates
    demand that our functions be symmetric around the origin, meaning
    that their derivatives must vanish at the origin.

    parameters
    ----------
    -- sign_a, integer-like, the sign of a. Should not be zero.
    """
    assert type(sign_a) == int
    assert sign_a != 0
    out = np.array([A_0*np.sign(sign_a),
                    K_RR_0])
    return out

def asymptotic_a_residual(r,a,da):
    """
    The asymptotic Robin-type boundary condition for a at infinity.
    Should vanish if the boundary condition is satisfied.

    Parameters
    ----------
    -- r, the radius
    -- a, the metric quantity,
    -- da, da/dr
    """
    return 2*r*da + a**3 - a

def asymptotic_lapse_residual(r,alpha,aleph):
    """
    The asymptotic Robin-type boundary condition for the lapse at infinity.
    Should vanish if the boundary condition is satisfied.

    Parameters
    ----------
    -- r, the radius
    -- alpha, the lapse
    -- aleph, the derivative of the lapse
    """
    return 1 - alpha - r*aleph

def origin_lapse_residual(alpha,aleph):
    """
    The boundary condition for the lapse at the origin. Should vanish if the
    boundary data is satisfied.

    Parameters
    ----------
    -- alpha, the lapse
    -- aleph, the derivative of the lapse
    """
    return aleph
    



# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
