#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 18:05:11 (jmiller)>

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

Here we define the data structure we will use. Our system uses a
3+1 split, so we make a distinction between our spatial variables,
the 3-metric and the extrinsic curvature, and our "purely" gauge
variables, the lapse and shift.

Our free parameters are the (r,r)-component of the 3-metric
and the (r,r)-component of the extrinsic curvature.

QUANTITIES:
-----------
-- hypersurface_vector, array-like. Contains metric quantity a,
   curvature quantity krr, lapse alpha, and dlapse/dr aleph:
   np.array([a,krr,alpha,aleph])
  -- a, the square root of the (r,r)-component of the metric.
  -- krr, extrinsic curvature (with one index up)
  -- alpha, the lapse
  -- aleph, the derivative of the lapse with respect to r.


-- matter_vector, array-like
   contains the relevant parts of the energy-momentum
   tensor:
   np.array([rho,jr,S])
   -- rho, the matter energy density
   -- jr, the radial component of the matter current (index up)
   -- S, the trace of the stress tensor.

We also define helper functions to generate the global adm quantities
such as the 3-metric, the 4-metric, the extrinsic curvature, etc.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # numpy arrays
# ----------------------------------------------------------------------

# indexes for hypersurface vector
NUM_HYPERSURFACE_VARIABLES = 2
A_I = 0
K_I = 1
HYPERSURFACE_NAMES=["a",r'$K^r_r$']

# indexes for matter vector
NUM_MATTER_VARIABLES=3
RHO_I = 0
J_I = 1
S_I = 2
MATTER_NAMES=[r'$\rho$',r'$j^r$',"S"]

# Gauge variables
ALPHA_I = 0
BETA_I = 1
GAUGE_NAMES = [r'$\alpha$',r'$\beta^r$']

# Helper functions to generate the global adm quantities.
# Not used in production code.
def get_3_metric(a,r,theta):
    """
    Given the (r,r)-component of the metric, a, the radial
    coordinate r, and the altitudinal coordinate theta,
    returns the matrix for the three-metric.
    """
    return np.diag([a**2,r**2,(r*np.sin(theta))**2])

def get_extrinsic_ud(Krr):
    """
    Given the (r,r)-component of the extrinsic curvature
    with one index up and one down, Krr, returns
    the extrinsic-curvature tensor with one index up
    and one index down.
    """
    return Krr*np.diag([1,-0.5,-0.5])

def get_four_metric(alpha,beta_r,a,r,theta):
    """
    Given the lapse alpha, the radial component of the shift beta_r,
    the (r,r)-component of the 3-metric, the radial coordinate r,
    and the altitudinal coordinate theta, returns the
    matrix of the four-metric with both indices down
    """
    gmunu = np.zeros((4,4))
    gamma = get_3_metric(a,r,theta)
    beta_up = np.array([beta_r,0,0])
    beta_down = np.dot(gamma,beta_up)
    gmunu[0,0] = -alpha**2 + beta_r**2
    gmunu[0,1:] = beta_up
    gmunu[1:,0] = beta_up
    gmunu[1:,1:] = gamma
    return gmunu

def get_shift(r,krr,alpha):
    """
    Returns the shift vector, as calculated algebraically given other
    quantities on the hypersurface

    Parameters
    ----------
    -- r, the radius
    -- krr, extrinsic curvature (with one index up)
    -- alpha, the lapse
    """
    return -0.5*alpha*r*krr


# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
