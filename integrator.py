#!/usr/bin/env python2

"""
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 18:49:39 (jmiller)>

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

This module defines the numerical integration routines that solve
for the numerical quantities on a hypersurface.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # numpy arrays
from scipy import integrate # numerical integration

# this program
import data_structures
import boundary_data
import matter
import rhs
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------


def solve_hypersurface(rcoords,matter_func,sign_a=1):
    """
    Given the matter content of a given spacelike hypersurface,
    generates the relevant quantities for that hypersurface,
    including metric quantities, curvature quantities, lapse,
    and shift.

    Returns these on a grid defined by rcoords

    Parameters
    ----------
    -- rcoords, array-like. Should be a list of r-coordinate values
       where the user wants to view the calculated quantities. Should range from
       r = 0 to r = rmax for some rmax.
       If rcoords[0] is not zero, it will be set to be.
       rcoords must be increasing with index
    -- matter_func, function. Should map radius r to a vector of matter
       quantities:
       r -> np.array([rho,jr,S])
       where:
       -- rho, the matter energy density
       -- jr, the radial component of the matter current (index up)
       -- S, the trace of the stress tensor.
    -- sign_a, integer, optional. The sign of a as defined in boundary_data
    """
    # ensure rcoords is what we expect
    rcoords[0] = 0
    assert np.all(rcoords[1:] > 0) and "r must be positive"
    assert reduce(lambda x,y: y > x, rcoords) and "r must be increasing"

    # initial data
    hypersurface0 = boundary_data.get_initial_hypersurface_vector(sign_a)
    # set up integrator
    integrator = integrate.ode(rhs.get_hypersurface_prime)
    integrator.set_f_params(matter_func)
    integrator.set_initial_value(hypersurface0)

    # output
    hypersurface_system = np.empty((len(rcoords),
                                    data_structures.NUM_HYPERSURFACE_VARIABLES))
    hypersurface_system[0] = hypersurface0

    # integrate
    for i,r in enumerate(rcoords[1:]):
        integrator.integrate(r)
        if not integrator.successful():
            print "Integration failure!"
            print "r = {}".format(integrator.t)
            print "Variables are:\n{}".format(integrator.y)
            raise ValueError("Integration Failed.")
        hypersurface_system[i+1] = integrator.y
    
    return hypersurface_system


# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
    
