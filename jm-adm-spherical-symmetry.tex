\documentclass[]{article}
% Time-stamp: <2015-03-29 20:59:58 (jmiller)>

% packages
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{mathrsfs}
\usepackage{verbatim}
\usepackage{braket}
\usepackage{listings}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}

% Preamble
\author{Jonah Miller}
\title{ADM Equations In Spherical Symmetry}

% Macros
\newcommand{\R}{\mathbb{R}}
\newcommand{\eval}{\biggr\rvert} %evaluated at
\newcommand{\N}{\mathbb{N}} % Integers
\newcommand{\myvec}[1]{\vec{#1}} % vectors for me
% total derivatives 
\newcommand{\diff}[2]{\frac{d #1}{d #2}} 
\newcommand{\dd}[1]{\frac{d}{d #1}}
% partial derivatives
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} 
\newcommand{\pdd}[1]{\frac{\partial}{\partial #1}} 
% second partial derivatives
\newcommand{\ppdd}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}
% vector calculus
\newcommand{\grad}{\myvec{\nabla}}
% quantum
\newcommand{\comm}[2]{\left[#1,#2\right]}
\newcommand{\norm}[1]{\left|#1\right|}
% special relativity
\newcommand{\ltran}[2]{\pd{\bar{x}^{#1}}{x^{#2}}}
% QFT
\newcommand{\PB}[2]{\comm{#1}{#2}_{PB}} % poisson bracket
\newcommand{\Ld}{\mathcal{L}}
\newcommand{\Hd}{\mathcal{H}}
% linear algebra
\newcommand{\minkowski}{\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}}
\newcommand{\fmunu}{\begin{pmatrix}
    0     & -E^1/c & -E^2/c & -E^3/c \\
    E^1/c & 0      & -B^3   & B^2    \\
    E^2/c & B^3    & 0      &-B^1    \\
    E^3/c & -B^2   & B^1    & 0
  \end{pmatrix}}
\newcommand{\gmunu}{\begin{pmatrix}
    0   & - B^1  & - B^2  & -B^3   \\
    B^1 & 0      & E^3/c  & -E^2/c \\
    B^2 & -E^3/c & 0      & E^1/c  \\
    B^3 & E^2/c  & -E^1/c & 0
  \end{pmatrix}}

% fractions
\newcommand{\half}{\frac{1}{2}}

% braces
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\sqrbrace}[1]{\left[ #1 \right]}
\newcommand{\curlybrace}[1]{\left\{ #1 \right\}}

% vectors
\newcommand{\vx}{\myvec{x}}
\newcommand{\vy}{\myvec{y}}
\newcommand{\vk}{\myvec{k}}
\newcommand{\vq}{\myvec{q}}
\newcommand{\vP}{\myvec{P}}

%operators
\newcommand{\ha}{\hat{a}}
\newcommand{\hb}{\hat{b}}
\newcommand{\hc}{\hat{c}}
\newcommand{\hd}{\hat{d}}
\newcommand{\hphi}{\hat{\phi}}
\newcommand{\hpi}{\hat{\pi}}
\newcommand{\hvphi}{\hat{\varphi}}
\newcommand{\hvpi}{\hat{\varpi}}
%conjugates
\newcommand{\had}{\hat{a}^\dagger}
\newcommand{\hbd}{\hb^\dagger}
\newcommand{\hcd}{\hc^\dagger}
\newcommand{\hdd}{\hd^\dagger}
\newcommand{\hphid}{\hat{phi}^\dagger}
\newcommand{\hpid}{\hat{pi}^\dagger}
\newcommand{\hvphid}{\hat{\varphi}^\dagger}
\newcommand{\hvpid}{\hat{\varpi}^\dagger}

% metric shorthands
\newcommand{\gij}{\gamma_{ij}}
\newcommand{\Kij}{K_{ij}}
\newcommand{\diag}{\text{diag}}

% controls
% \renewcommand\thesubsection{\thesection.\alph{subsection}}

\begin{document}
\maketitle

\section{Choose an ansatz for $g_{ij}$ and $K_{ij}$ for spherical
  symmetry.}
\label{sec:ansatz}

As suggested, we use the (3+1)-split and choose the 3-metric ansatz
\begin{equation}
  \label{eq:3:metric:ansatz}
  ^{(3)}ds^2 = a^2(t,r) dr^2 + b^2(t,r) r^2 d\Omega^2,
\end{equation}
where $a$ and $b$ are as-of-yet undetermined functions. We choose the
azimuthal factor to be $b^2 r^2$ so that $b$ is allowed to be finite
and regular at the origin (which is a coordinate singularity). This
means that, in matrix form, the metric and its inverse are:
\begin{eqnarray}
  \label{eq:metric:matrix}
  \gamma &=& \diag\left(a^2, r^2 b^2, r^2 b^2 \sin^2(\theta)\right)\\
  \label{eq:metric:inverse:matrix}
  \gamma^{-1} &=& \diag\left(\frac{1}{a^2},\frac{1}{r^2b^2},\frac{1}{r^2b^2\sin^2(\theta)}\right).
\end{eqnarray}

As suggested, to simplify our equations, we work with the extrinsic
curvature with mixed indices:
\begin{equation}
  \label{eq:def:raised:extrinsic:1}
  K^i_{\ j} = g^{ia}K_{aj}
\end{equation}
and we again choose the spherical symmetry ansatz:
\begin{equation}
  \label{eq:def:raised:extrinsic:2}
  K^i_{\ j} = \diag\left(K^r_{\ r},K^{\theta}_{\ \theta},K^{\phi}_{\ \phi}\right)
\end{equation}
However, since the system is spherically symmetric, parallel transport
in the $\theta$ direction should have the same effect as parallel
transport in the $\phi$ direction. Therefore $K^\theta_{\ \theta} =
K^{\phi}_{\ \phi}$ and
\begin{equation}
  \label{eq:def:raised:extrinsic}
  K^i_j = \diag\left(K^r_{r},K^\theta_{\theta},K^\theta_{\theta}\right),
\end{equation}
where we have now neglected to order the indices since $K$ is
symmetric \cite{Alcubierre}.\footnote{Alcubierre makes this argument
  in section 10.3, where he briefly discusses numerical relativity in
  spherical symmetry.}

\section{Important Quantities}
\label{sec:quantities}

To work out the ADM equations in our ansatz, we need to calculate a
few things. For clarity, we perform these calculations now before
continuing with the assignment.

\subsection{Metric Quantities}
First we calculate the metric-compatible connection and the Riemann
curvature tensor and its contractions for our hypersurface.

The 3D Christoffel symbols are defined as:
\begin{equation}
  \label{eq:def:christoffel}
  ^{(3)}\Gamma^{a}_{bc} = \frac{1}{2}\gamma^{ai}\left[\partial_c \gamma_{bi} + \partial_b \gamma_{ci} - \partial_i \gamma_{bc}\right].
\end{equation}
Since $\gamma$ is diagonal, this simplifies to
\begin{equation}
  \label{eq:def:christoffel:1}
  ^{(3)}\Gamma^a_{bc} = \frac{1}{2}\gamma^{aa}\left[\partial_c\gamma_{ba} + \partial_b\gamma_{ca} - \partial_a \gamma_{bc}\right].
\end{equation}
Because of the assumption of spherical symmetry, which is quite
strong, most of the connection coefficients will vanish. However, even
with the simplification of equation \eqref{eq:def:christoffel:1}, it's
quite tedious to calculate them all. Therefore, I calculated the
coefficients using the xAct \cite{xAct1} tensors \cite{xAct2,xAct3}
packages for Mathematica \cite{Mathematica}.\footnote{A printout of the code used can be found in section \ref{sec:math:code}} The nonzero coefficients
are:
\begin{eqnarray}
  \label{eq:nonzero:christoffel:111}
  ^{(3)}\Gamma^1_{11} &=& \frac{a'}{a}\\
  \label{eq:nonzero:christoffel:122}
  ^{(3)}\Gamma^1_{22} &=& -\frac{1}{a^2}\left[b r \left(b + r b'\right)\right]\\
  \label{eq:nonzero:christoffel:133}
  ^{(3)}\Gamma^1_{33} &=& \sin^2(\theta) ^{(3)}\Gamma^1_{22}\\
  \label{eq:nonzero:christoffel:212}
  ^{(3)}\Gamma^2_{12} = ^{(3)}\Gamma^2_{21} = ^{(3)}\Gamma^3_{31} = ^{(3)}\Gamma^3_{13} &=& \frac{1}{r} + \frac{b'}{b}\\
  \label{eq:nonzero:christoffel:233}
  ^{(3)}\Gamma^2_{33} &=& -\cos(\theta)\sin(\theta)\\
  \label{eq:nonzero:christoffel:323}
  ^{(3)}\Gamma^3_{32} = ^{(3)}\Gamma^3_{23} &=& \cot(\theta)
\end{eqnarray}
where we have used $a'$ to denote $\partial_r a$ and (when the time
comes) we will denote $\dot{a}$ to denote $\partial_t a$.

Similarly, the nonzero components of the 3D Riemann tensor
are:\footnote{We now suppress the $^{(3)}R$ superscript notation and
  only invoke it when the object lacks indices. Instead, we use latin
  indices to denote 3D objects and greek indices to denote 4D
  objects.}
\begin{eqnarray}
  \label{eq:nonzero:riemann:1212}
  -R_{2112}=-R_{1221}=R_{1212}=R_{2121} &=& \frac{rb}{a}\left[a'(b + rb')-2 a b' - a r b''\right]\\
  \label{eq:nonzero:riemann:1313}
  -R_{3113} = -R_{1331}=R_{1313} = R_{3131} &=& \sin^2(\theta) R_{2121}\\%\frac{r b \sin^2(\theta)}{a}\left[a'(b + r b') - 2 a b' - a r b''\right]
  \label{eq:nonzero:riemann:2323}
  -R_{2332} = -R_{3223} = R_{3232}=R_{2323} &=& \frac{b^2}{a^2} r^2 \sin^2(\theta)\left[a^2 - (b +r b')^2\right]
\end{eqnarray}
where the other components vanish by a vanishing of the Christoffel
coefficients, vanish by antisymmetry, or are accounted for by
symmetry.

We can now calculate the Ricci tensor $R_{ij} = R^k_{\ ikj}$. xAct
tells us that it's diagonal with the following nonzero components:
\begin{eqnarray}
  \label{eq:ricci:11}
  R_{11} &=& \frac{2}{abr}\left\{a'\left[(b + rb') - a r b''\right]-2ab'\right\}\\
  \label{eq:ricci:22}
  R_{22} &=& 1 + \frac{1}{a^3}\left[b r a' (b + r b')\right] + \frac{1}{a^2}\left[b^2 + r^2 b' + b r (4 b' + r b'')\right]\\
  \label{eq:ricci:33}
  R_{33} &=& \sin^2(\theta) R_{22}.
\end{eqnarray}
This means that we can finally calculate the Ricci scalar, $^{(3)}R=R^i_{\ i}$, which turns out to be
\begin{equation}
  \label{eq:ricci:scalar}
  ^{(3)}R = \frac{a}{a^2 b^2 r^2}\left\{a^3 + 2 a'b r(b + r b')
  - a \left[b^2 + r^2 (b')^2 + 2 b r \left(3 b' +r b''\right)\right]\right\}.
\end{equation}

\subsection{Curvature Properties}

These calculations are substantially simpler, but we'll benefit from
doing them. First, the trace of the extrinsic curvature is:
\begin{equation}
  \label{eq:trace:K}
  K = K^i_i = K^r_r +2 K^\theta_\theta
\end{equation}
and its contraction with itself is
\begin{eqnarray}
  \label{eq:KijKij}
  K_{ij}K^{ij} &=& K_i^j K^i_j \nonumber\\
  &=& \sum_i \sum_j \delta_{ij} K^i_i \delta_{ij}K^j_j\text{ because }K\text{ is diagonal}\nonumber\\
  &=& \sum_i (K^i_i)^2 \nonumber\\
  &=&  (K^r_r)^2 + 2(K^\theta_\theta)^2 
\end{eqnarray}

For convenience, we also write down the index-raised curvature tensor:
\begin{equation}
  \label{eq:Kuu}
  K^{ij} = \diag\left(\frac{K^r_r}{a^2}, \frac{K^\theta_\theta}{r^2b^2},\frac{\csc^2(\theta)K^\phi_\phi}{b^2 r^2}\right).
\end{equation}

\section{Write down the constraint equations in this ansatz.}
\label{sec:constraints}

Before assuming spherical symmetry, the constraint equations in the
ADM formalism take the form:
\begin{eqnarray}
  \label{eq:hamiltonian:constraint:adm}
  ^{(3)}R+K^2 - K_{ij}K^{ij} &=& 16\pi\rho\\
  \label{eq:momentum:constraint:adm:all}
  D_j(K^{ij} - \gamma^{ij}K) &=& 8\pi j^i,\nonumber
\end{eqnarray}
where $D_i$ is the covariant derivative in the hypersurface, $\rho =
n^\mu n^\nu T_{\mu\nu}$ and $j^i = -P^{i\mu}n^\nu T_{\mu\nu}.$ 

\subsection{The Hamiltonian Constraint}

With the definitions of the curvature tensors above, we can write down
the Hamiltonian constraint with minimal effort (but a lot of writing):
\begin{eqnarray}
  \label{eq:Hamiltonian:constraint:unsimplified}
  16\pi\rho &=& \frac{a}{a^2 b^2 r^2}\left\{a^3 + 2 a'b r(b + r b')
    - a \left[b^2 + r^2 (b')^2 + 2 b r \left(3 b' +r b''\right)\right]\right\}\\
  &&\quad + \left(K^r_r + 2 K^\theta_\theta\right)^2 + (K^r_r)^2 + 2(K^\theta_\theta)^2.\nonumber
\end{eqnarray}


\subsection{The Momentum Constraint}

Note that angular momentum depends on distance from a central axis and
thus requires \textit{axisymmetry} as opposed to \textit{spherical}
symmetry. Thus, $j^\theta$ and $j^\phi$ must vanish identically and we
only need the radial constraint equation:
\begin{displaymath}
  D_j(K^{rj} - \gamma^{rj}K) = 8\pi j^r.
\end{displaymath}
If we expand the contraction with the covariant derivative, we find that
\begin{displaymath}
  8\pi j^r = \partial_j\left(K^{rj} - \gamma^{rj}K\right) + \Gamma^r_{jd}\left(K^{dj} - \gamma^{dj}K\right) + \Gamma^j_{jd}\left(K^{rd} - \gamma^{rd}K\right).
%  
%\Rightarrow 8\pi j^r &=& \left(\partial_r + 2\frac{a'}{a}\right)\left[\frac{K^r_r}{a^2} - \frac{1}{a^2}\left(K^r_r + 2 K^\theta_\theta\right)\right].
\end{displaymath}
Contracting over the nonzero Christoffel symbols, this becomes
\begin{eqnarray}
  8\pi j^r &=& \partial_r \left(K^{rr} - \gamma^{rr} K\right) + \Gamma^r_{rr}\left(K^{rr} - \gamma^{rr} K\right) + \Gamma^r_{\theta\theta}\left(K^{\theta\theta} -\gamma^{\theta\theta}K\right) + \Gamma^r_{\phi\phi}\left(K^{\phi\phi} - \gamma^{\phi\phi}K\right)\nonumber\\
  &&\qquad + \left(\Gamma^r_{rr} + \Gamma^\theta_{\theta r} + \Gamma^\phi_{\phi r} \right)\left(K^{rr} - \gamma^{rr}K\right)\nonumber\\
  &=& \partial_r \left(\gamma^{rr}K^r_r - \gamma^{rr} K\right) + \Gamma^r_{\theta\theta}\gamma^{\theta\theta}\left(K^{\theta}_\theta -K\right) + \Gamma^r_{\phi\phi}\gamma^{\phi\phi}\left(K^{\phi}_\phi - K\right)
  + 2\left(\Gamma^r_{rr} +  \Gamma^\theta_{\theta r}\right)\gamma^{rr}\left(K^{r}_r -  K\right)\nonumber\\
  \Rightarrow 8\pi j^r &=& \partial_r \left(\gamma^{rr}K^r_r - \gamma^{rr} K\right) + \left(\Gamma^r_{\theta\theta}\gamma^{\theta\theta}+ \Gamma^r_{\phi\phi}\gamma^{\phi\phi}\right)\left(K^{\theta}_\theta -K\right) 
  + 2\left(\Gamma^r_{rr} +  \Gamma^\theta_{\theta r}\right)\gamma^{rr}\left(K^{r}_r -  K\right).\nonumber
\end{eqnarray}
And if we plug in the metric and connection terms, the momentum constraint becomes:
\begin{eqnarray}
  8\pi j^r &=& \partial_r \left[\frac{1}{a^2}\left(K^r_r - K\right)\right]\nonumber\\
  &&\qquad- \left[\frac{1}{a^2}\left(b r(b + r b')\right)\left(\frac{1}{r^2 b^2}\right) + \frac{1}{a^2}\left(b r \sin^2(\theta)(b + r b')\right)\left(\frac{1}{r^2 b^2 \sin^2(\theta)}\right)\right]\left(K^\theta_\theta - K\right)\nonumber\\
  &&\qquad  + 2 \left[\frac{a'}{a} + \left(\frac{1}{r} + \frac{b'}{b}\right)\right]\left(\frac{1}{a^2}\right)\left(K^r_r - K\right)\nonumber\\
  &=&  \partial_r \left[\frac{1}{a^2}\left(K^r_r - K\right)\right] - \frac{2}{a^2 r b}(b + r b')\left(K^\theta_\theta - K\right) + \frac{2}{a^2}\left(\frac{a'}{a} + \frac{1}{r} + \frac{b'}{b}\right)\left(K^r_r - K\right)\nonumber\\
  &=&  \frac{1}{a^2}\left(\partial_r K^r_r\right) - \frac{2}{a^2}\left(\frac{a'}{a}\right)- \frac{2}{a^2 r b}(b + r b')\left(K^\theta_\theta - K\right) + \frac{2}{a^2}\left(\frac{a'}{a} + \frac{1}{r} + \frac{b'}{b}\right)\left(K^r_r - K\right)\nonumber\\
  \label{eq:momentum:constraint:unsimplified}
\Rightarrow 8\pi j^r &=& \frac{1}{a^2}\left(\partial_r K^r_r\right) - \frac{2}{a^2 r b}(b+rb')\left(K^\theta_\theta - K\right) + \frac{2}{a^2}\left(\frac{1}{r} + \frac{b'}{b}\right)\left(K^r_r - K\right).
\end{eqnarray}

\section{Write Down Gauge Conditions}
\label{sec:gauge}
As suggested, we choose maximal slicing
\begin{equation}
  \label{eq:maximal:slicing}
  K=0
\end{equation}
and areal coordinates 
\begin{equation}
  \label{eq:areal:coordinates}
  \int_{\Omega} d\Omega b^2 r^2 = 4\pi r^2.
\end{equation}
These translate to the conditions that
\begin{equation}
  \label{eq:maximal:slicing:implication}
  K^r_r = - 2 K^\theta_\theta
\end{equation}
and
\begin{equation}
  \label{eq:areal:coordinates:implication}
  b(t,r) = 1\ \forall\ t,r
\end{equation}
respectively.

\section{Combine Gauge Conditions and Constraints into a Standard
  Form}
\label{sec:standard:form}

Let's look at the Hamiltonian constraint (equation
\eqref{eq:Hamiltonian:constraint:unsimplified}) first. By demanding
maximal slicing, we see that the formula simplifies to:
\begin{eqnarray}
  16\pi\rho &=& \frac{a}{a^2 b^2 r^2}\left\{a^3 + 2 a'b r(b + r b')
    - a \left[b^2 + r^2 (b')^2 + 2 b r \left(3 b' +r b''\right)\right]\right\}\nonumber\\
  &&\quad + (K^r_r)^2 + 2(-\frac{1}{2}K^r_r)^2.\nonumber\\
  &=& \frac{a}{a^2 b^2 r^2}\left\{a^3 + 2 a'b r(b + r b')
    - a \left[b^2 + r^2 (b')^2 + 2 b r \left(3 b' +r b''\right)\right]\right\}
  + \frac{3}{2} (K^r_r)^2 .\nonumber
\end{eqnarray}
Then, because $b=1$ everywhere, $b'=0$ as well, leading to the following simplification:
\begin{equation}
  \label{eq:hamiltonian:constraint;gauges:applied}
  16\pi\rho = \frac{1}{a r^2}\left[a^2 + 2 a' r - a\right] + \frac{3}{2}(K^r_r)^2.
\end{equation}

In the momentum constraint, the $K$ term vanishes, leaving
\begin{displaymath}
  8\pi j^r = \frac{1}{a^2}\left(\partial_r K^r_r\right) - \frac{2}{a^2 r b}(b+rb')\left(K^\theta_\theta \right) + \frac{2}{a^2}\left(\frac{1}{r} + \frac{b'}{b}\right)\left(K^r_r \right).
\end{displaymath}
Then, because $b=1$ and $b'=0$, we get
\begin{eqnarray}
  8\pi j^r &=& \frac{1}{a^2}\left(\partial_r K^r_r\right) - \frac{2}{a^2r}K^\theta_\theta  + \frac{2}{a^2r}K^r_r \nonumber\\
   &=& \frac{1}{a^2}\left(\partial_r K^r_r\right) + \frac{1}{a^2r}K^r_r  + \frac{2}{a^2r}K^r_r \nonumber\\
  \label{eq:momentum:constraint:gauges:applied}
  \Rightarrow 8\pi j^r &=& \frac{1}{a^2}\left(\partial_r K^r_r\right) + \frac{3}{a^2r}K^r_r .
\end{eqnarray}

Finally, we take equations
\eqref{eq:hamiltonian:constraint;gauges:applied} and
\eqref{eq:momentum:constraint:gauges:applied} and arrange them in the
``canonical'' form with derivatives on the lefthand side and everything else
on the righthand side:
\begin{eqnarray}
  \label{eq:constraints:canonical:1}
  \frac{\partial}{\partial r}a(t,r) &=&\frac{a}{2r}\left(r^2\left[16\pi \rho - \frac{3}{2}(K^r_r)^2\right] - a + 1\right)\\
  \label{eq:constraints:canonical:2}
  \frac{\partial}{\partial r} K^r_r(t,r) &=& 8\pi a^2 j^r - \frac{3}{r} K^r_r.
\end{eqnarray}

I'm a little concerned about the fact that I have $1/r$ terms,
implying the equations may be ill-behaved at the origin. I'm not sure
how best to regularize them, though.

\section{Find Boundary Conditions}
\label{sec:boundary:conditions}

To satisfy spherical symmetry, we want to demand that the spacetime by
symmetric about the origin $r=0$. For physical reasons, we want to
demand that the spacetime is asymptotically flat. 

Symmetry about the origin implies
\begin{eqnarray}
  \partial_r a(t,r)\eval_{r=0} &=& 0\\
  \text{and }\partial_r K^r_r(t,r)\eval_{r=0} &=& 0,
\end{eqnarray}
since this means we can cleanly construct an even solution for $a$ and
$K^r_r$.

For asymptotically flat spacetimes, we should assume
that for large $r$ we approach the Schwarzschild solution
\cite{Alcubierre},
\begin{displaymath}
  \label{eq:schwarzschild:metric}
  ds^2 = -\left( 1 - \frac{r_s}{r} \right) dt^2 + \left(1 - \frac{r_s}{r}\right)^{-1} dr^2 - r^2 (d\theta^2 +\sin^2(\theta)d\phi^2.
\end{displaymath}
The constant time slices of Schwarzschild have vanishing extrinsic
curvature \cite{Alcubierre}, so one condition we could demand is that
\begin{equation}
  \lim_{r\to\infty} K^r_r = 0.
\end{equation}

However, since we won't actually reach infinity, this condition isn't
terribly useful. Better if we had a functional dependence of $r$ to
rely on. Fortunately, we can rely on the 3-metric. The radial term,
$a(t,r)$, should behave asymptocally as
\begin{displaymath}
  \lim_{r\to\infty} a(t,r) = \left(1 - \frac{r_s}{r}\right)^{-1/2},
\end{displaymath}
where the square root comes from the fact that $a^2$ appears in the
line element, not $a$. Unfortunately, we don't know what $r_s$ should
be in this case, so we need to eliminate it somehow. Here we follow
the method Alcubierre \cite{Alcubierre} describes on page 126. To
simplify notation, we denote the asymptotic value of $a$ by
$a_\infty$. First we take a derivative to attain
$$\partial_ra_\infty = - \left(\frac{1}{2}\right)\left(\frac{r_s}{r^2}\right)\left(1-\frac{r_s}{r}\right)^{-3/2}$$
We then recognize that\footnote{checked with Mathematica \cite{Mathematica}}
\begin{displaymath}
  \frac{1}{2}\left(1 - a_\infty^2\right) = \frac{r}{a_\infty}\partial_r a_\infty
\end{displaymath}
or
\begin{equation}
  \label{eq:a:asymptotic:condition}
  \partial_r a_\infty = \frac{1}{2r} \left(a_\infty - a_\infty^3\right),
\end{equation}
which is \textit{Robin} type boundary condition that we can use for
the outer boundary.

In this section, (excluding the vanishing curvature at infinity) we
have found \textit{three} boundary conditions to constrain
\textit{two} variables. Therefore we only need to use two of the three
conditions. It is not clear to me which two are best.

\section{Find Equations for the Lapse $\alpha$ and the Shift
  $\beta_i$}
\label{sec:lapse:shift}

Consider ADM evolution equations for the metric and
extrinsic curvature respectively \cite{Alcubierre}:\footnote{Recall that in ADM coordinates $\partial_t = \mathcal{L}_{\myvec{t}}$.}
\begin{eqnarray}
  \label{eq:adm:metric}
  \left(\partial_t - \mathcal{L}_{\myvec{\beta}}\right) \gamma_{ij} &=& - 2\alpha K_{ij}\\
  \label{eq:adm:extrinsic}
  \left(\partial_t - \mathcal{L}_{\myvec{\beta}}\right) K_{ij} &=&  - D_i D_j\alpha
  + \alpha\left[^{(3)}R_{ij} + K K_{ij} - 2 K_{ik} K^k_j\right]
  + 4\pi \alpha \left[\gamma_{ij}(S-\rho) - 2 S_{ij}\right],
\end{eqnarray}
where $S_{ij}$ is the stress tensor,
$$S_{ij} = P_i^\mu P_j^\nu T_{\mu\nu}.$$
We will use these equations to attain equations of the lapse and shift given our chosen gauge conditions. 

\subsection{Lapse}

To attain an equation for the evolution of the evolution of the trace
of the extrinsic curvature, we take the trace of both sides of
equation \eqref{eq:adm:extrinsic}. Before we perform the operation,
however, note that
\begin{eqnarray}
  \left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right) K &=& \left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right)\gamma^{ij}K_{ij}\nonumber\\
  &=& \gamma^{ij}\left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right)K_{ij} + K_{ij}\left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right)\gamma^{ij}\nonumber\\
  \label{eq:trace:K:derivative:lemma}
  &=& \gamma^{ij}\left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right)K_{ij} - 2\alpha K_{ij} K^{ij}\text{ by equation \eqref{eq:adm:metric}.}
\end{eqnarray}
We will use this below. Now, without further ado, we take the trace:
\begin{eqnarray}
  \gamma^{ij}\left(\partial_t - \mathcal{L}_{\myvec{\beta}}\right) K_{ij} &=&  - D^2\alpha
  + \alpha\left[^{(3)}R + K^2 - 2 K_{ij} K^{ij}\right]
  + 4\pi \alpha \left[3(S-\rho) - 2 S\right]\nonumber\\
  \Rightarrow \left(\partial_t + \mathcal{L}_{\myvec{\beta}}\right)K &=&  - D^2\alpha
  + \alpha\left[^{(3)}R + K^2 \right]
  + 4\pi \alpha \left[3(S-\rho) - 2 S\right]\nonumber\\
  \Rightarrow \partial_t K -\beta^i\partial_i K  &=&  - D^2\alpha
  + \alpha\left[^{(3)}R + K^2 \right]
  + 4\pi \alpha \left(S -3 \rho\right),\nonumber
\end{eqnarray}
where we have used equation \eqref{eq:trace:K:derivative:lemma} to
commute the metric through the Lie derivative and where $S$ is the
trace of the stress tensor. We now use the Hamiltonian constraint
\eqref{eq:hamiltonian:constraint:adm} to eliminate the Ricci scalar:
\begin{eqnarray}
    \Rightarrow \partial_t K -\beta^i\partial_i K  &=&  - D^2\alpha
  + \alpha\left[16\pi\rho + K_{ij}K^{ij} - K^2 + K^2 \right]
  + 4\pi \alpha \left(S -3 \rho\right)\nonumber\\
  &=& -D^2 \alpha +\alpha \left[16\pi\rho - 12\pi\rho + 4\pi S + K_{ij}K^{ij}\right]\nonumber\\
  \label{eq:trace:K:evolution:adm}
  &=& -D^2\alpha + \alpha\left[K_{ij}K^{ij} + 4\pi(\rho+S)\right].
\end{eqnarray}
Finally, we allow $K$ to vanish as per the maximal slicing condition to find
\begin{displaymath}
  D^2\alpha = \alpha \left[ K_{ij} K^{ij} + 4\pi (\rho + S)\right],
\end{displaymath}
which is the same term as Alcubierre gets \cite{Alcubierre} on page
124. In our case, 
$$K_{ij} K^{ij} = (K^r_r)^2 + 2 (K_\theta^\theta)^2 = \frac{3}{2}(K^r_r)^2,$$
and the metric coefficients are known, so the Laplacian operator\footnote{This is a Laplacian since the metric is Euclidean signature} $D^2\alpha$
becomes:
\begin{eqnarray}
  D^2\alpha &=& D^i \left[D_i\alpha\right] = \gamma^{ij}D_j\left[\partial_i \alpha\right]\nonumber\\
  &=& \gamma^{ij}\left(\partial_j \partial_i \alpha- \Gamma_{ij}^k \partial_k\alpha \right)\nonumber\\
  &=& \sum_{i} \gamma^{ii}\left(\partial_i^2 \alpha - \Gamma_{ii}^k\partial_k\alpha\right)\nonumber\\
  &=& \left[\sum_{i}\gamma^{ii}\partial_i^2 \alpha\right] - \sum_{i}\left[\sum_k \gamma^{ii}\Gamma_{ii}^k\partial_k \alpha\right]\nonumber\\
  &=& \frac{1}{a^2}\partial_r^2\alpha - \sum_i\gamma^{ii}\Gamma^r_{ii}\partial_r\alpha\text{ because }\partial_i\alpha = 0 \text{ if }i \neq  0\nonumber\\
  &=& \frac{1}{a^2}\partial_r^2\alpha - \left[\frac{1}{a^2}\frac{a'}{a} + \frac{1}{r^2}\left(-\frac{1}{a^2} r\right) + \frac{1}{r^2\sin^2(\theta)}\left(-\frac{\sin^2(\theta)}{a^2}r\right)\right]\partial_r\alpha\nonumber\\
  &=& \frac{1}{a^2} \partial_r^2\alpha - \frac{a'}{a^3} \partial_r \alpha + \frac{2}{r}\frac{1}{a^2}\partial_r\alpha.
\end{eqnarray}
so the equation simplifies to
\begin{displaymath}
\frac{1}{a^2} \partial_r^2\alpha - \frac{a'}{a^3} \partial_r \alpha + \frac{2}{r}\frac{1}{a^2}\partial_r\alpha = \alpha \left[ \frac{3}{2}\left(K^r_r\right)^2 + 4\pi (\rho + S)\right].
\end{displaymath}
% \begin{displaymath}
%   \frac{1}{a}\left[\partial_r\left(\frac{1}{a}\partial_r\alpha\right) + \frac{2}{r}\left(\frac{1}{a}\partial_r\alpha\right)\right] = \alpha \left[ \frac{3}{2}\left(K^r_r\right)^2 + 4\pi (\rho + S)\right].
% \end{displaymath}

% \subsection{Numerically Stable Form}
% 
% The above form is similar to an ODE whose solution is exponential growth:
% $$y''(t) = c^2 y(t).$$
% To avoid this, we recast our ODE as an equation for a different variable,
% \begin{equation}
%   \label{eq:def:lapse:tilde}
%   \tilde{\alpha} = \ln(\alpha)
% \end{equation}
% meaning
% \begin{equation}
%   \label{eq:deriv:tilde:alpha}
%   \partial_r \tilde{\alpha} = \frac{1}{\alpha}\partial_r\alpha.
% \end{equation}
% Thus, our equation for the lapse becomes
% \begin{equation}
%   \label{eq:loglapse:elliptoic}
%   \frac{1}{a}\left[\partial_r\left(\frac{1}{a}\partial_r\alpha\right) + \frac{2}{r}\left(\frac{1}{a}\partial_r\alpha\right)\right] = \left[ \frac{3}{2}\left(K^r_r\right)^2 + 4\pi (\rho + S)\right].
% \end{equation}

\subsubsection{First-Order Form}
It will pay later to have the equations for the lapse in a first-order form, so we introduce the auxiliary variable
\begin{equation}
  \label{eq:def:grad:lapse}
  \aleph := \frac{1}{a}\partial_r \alpha.
\end{equation}
Then our equation for the lapse becomes
\begin{eqnarray}
  \label{eq:lapse:equation:1}
  \partial_r \alpha &=& a \aleph\\
  \label{eq:lapse:equation:2}
  \partial_r \aleph &=& a\alpha \left[\frac{3}{2}(K^r_r)^2 + 4\pi(\rho +S)\right] - \frac{2}{r}\aleph.
\end{eqnarray}

\subsubsection{Boundary Conditions}

Our boundary conditions for the spacetime transfer simply to the
lapse. Spherical symmetry means that $\aleph$ must vanish at the
origin while asymptotically flat means that $\alpha$ must fall off as
$1-c/r$ for some $c$ and large $r$ \cite{Alcubierre}. As with the
metric, we eliminate the unknown constant by taking a derivative and
relating it to the lapse \cite{Alcubierre}. This leads to the
following two boundary conditions:
\begin{eqnarray}
  \label{eq:lapse:boundary:conditions:origin}
  \aleph(r=0) &=& 0\\
  \label{eq:lapse:boundary:conditions:infinity}
  a \aleph_\infty &=& \frac{1-\alpha_\infty}{r}
\end{eqnarray}
where again the $\infty$ subscript means asymptotic behaviour.

\subsection{Shift}

Now let us examine the ADM evolution equation for the metric, equation
\eqref{eq:adm:metric}. If we expand out the Lie derivative into
covariant deriavatives (and then into partial derivatives and
connection terms) we find:
\begin{equation}
  \label{eq:adm:evolution:expanded}
  \partial_t \gamma_{ij} = -2\alpha \gamma_{ik}K^k_j 
  +\beta^k\partial_k\gamma_{ij} + \gamma_{ik}\partial_j\beta^k 
  + \gamma_{kj}\partial_i\beta^k
\end{equation}
Consider the $i=j=\theta$ term:
\begin{eqnarray}
  \partial_t\gamma_{\theta\theta} &=&-2\alpha \gamma_{\theta k}K^k_\theta
  +\beta^k\partial_k\gamma_{\theta\theta} + \gamma_{\theta k}\partial_\theta\beta^k 
  + \gamma_{k\theta}\partial_\theta\beta^k\nonumber\\
  &=& -2\alpha \gamma_{\theta k}K^k_\theta
  +\beta^k\partial_k\gamma_{\theta\theta} \text{ by spherical symmetry}\nonumber\\
  &=& -2\alpha \gamma_{\theta \theta}K^\theta_\theta
  +\beta^k\partial_k\gamma_{\theta\theta}\nonumber\\
  \Rightarrow \partial_t(r^2b^2) &=& -2\alpha r^2b^2 \left(-\frac{K^r_r}{2}\right) + \beta^k\partial_k (r^2b^2),\nonumber
\end{eqnarray}
but by our areal coordinates condition, $b=1$. Therefore, the lefthand
side vanishes and we have:
\begin{eqnarray}
  \beta^k\partial_k r^2 &=& -\alpha r^2 K^r_r\nonumber\\
  \Rightarrow \beta^r\partial_r r^2 &=& -\alpha r^2 K^r_r\nonumber\\
  \Rightarrow 2 r \beta^r &=& -\alpha r^2 K^r_r\nonumber\\
  \label{eq:shift:solution}
  \Rightarrow \beta^r &=& -\frac{1}{2}\alpha r K^r_r.
\end{eqnarray}

\section{Summary}

For convenience, I include all of the equations required to solve GR
in spherical symmetry in one place. The metric and curvature are given by:
\begin{eqnarray}
  \label{eq:metric:quantities:summary}
  \gamma_{ij} &=& \diag\left(a^2, r^2, r^2 \sin^2(\theta)\right)\\
  K^i_j &=& K^r_r\diag\left(1,-\frac{1}{2},-\frac{1}{2}\right)
\end{eqnarray}
which can be solved by:
\begin{eqnarray}
  \label{eq:equations:summary}
  \partial_ra &=&\frac{a}{2r}\left(r^2\left[16\pi \rho - \frac{3}{2}(K^r_r)^2\right] - a + 1\right)\\
  \partial_r K^r_r &=& 8\pi a^2 j^r - \frac{3}{r} K^r_r\\
  \partial_r \alpha &=& \aleph\\
  \partial_r \aleph &=& a^2\alpha \left[\frac{3}{2}(K^r_r)^2 + 4\pi(\rho +S)\right] - \frac{a'}{a}\aleph\\
  \beta^r &=& -\frac{1}{2}\alpha r K^r_r,
\end{eqnarray}
subject to the boundary conditions:
\begin{eqnarray}
  \label{eq:boundary:conditions:summary}
  \displaystyle
  \partial_r a(t,r=0) = 0 & \partial_r a_\infty = \cfrac{a_\infty - a_\infty^3}{2r}\\
  \partial_r K^r_r(t,r=0) = 0 & \lim_{r\to\infty} K^r_r = 0  \\
  \aleph(t,r=0) = 0 &a \aleph_\infty = \cfrac{1-\alpha_\infty}{r}
\end{eqnarray}

\appendix
\section{Mathematica Code}
\label{sec:math:code}

\includepdf[pages={-}]{xact-adm-spherical-symmetry.pdf}

%Bibliography
\bibliography{jm-adm-spherical-symmetry}
\bibliographystyle{hunsrt}

\end{document}

% random crap

% Let's calculate the nonzero coefficients one by one. First note that:
% \Begin{eqnarray}
%   \Gamma^\alpha_{\sigma\sigma} &=& \frac{1}{2}g^{\alpha\mu}\left[\partial_\sigma g_{\sigma\mu} + \partial_\sigma g_{\sigma\mu} - \partial_\mu g_{\sigma\sigma}\right]\nonumber\\
%   &=& g^{\alpha\mu}\left[\partial_\sigma g_{\sigma\mu} - \frac{1}{2} \partial_\mu g_{\sigma\sigma}\right].\nonumber
% \end{eqnarray}
% 
% Let's calculate the simplest case first:
% \begin{eqnarray}
%   \Gamma^0_{00} &=& g^{0\mu}\left[\partial_0 g_{0\mu} - \frac{1}{2}\partial_\mu g_{00}\right]\nonumber\\
%   &=& -\left(\frac{1}{\alpha^2}\right)\left[\partial_t (-\alpha^2 + \beta^2) - \frac{1}{2}\partial_t\left(-\alpha^2 + \beta^2\right)\right] + \left(\frac{\beta^i}{\alpha^2}\right)\left[\partial_t \beta_i - \frac{1}{2}\partial_i\left(-\alpha^2+\beta^2\right)\right]\nonumber\\
%   \label{eq:gamma:0:00}
% \Rightarrow \Gamma^0_{00} &=& \frac{1}{2\alpha^2} \left[2\partial_t \left(\alpha^2-\beta^2\right) + 2\beta^i\partial_t\beta_i + \beta^r\partial_r\left(\alpha^2 - \beta^2\right)\right],
% \end{eqnarray}
% because derivatives of the lapse and shift with respect to $\theta$
% and $\phi$ must vanish to preserve spherical symmetry.
% 
% Next, we calculate:
% \begin{eqnarray}
%   
% \end{eqnarray}
% The full four-dimensional metric includes
% the lapse and shift:\footnote{Note that the lapse and shift can only
%   be functions of $r$ and $t$. Otherwise, spherical symmetry would be
%   violated.}
% \begin{eqnarray}
%   \label{eq:metric:4D:def}
%   ds^2 = (-\alpha^2+ \beta_i\beta^i)dt^2 + 2 \beta_i dt dx^i + a^2 dr^2 + b^2 r^2 d\Omega^2
% \end{eqnarray}
% So the metric and inverse four-metric are:
% \begin{eqnarray}
%   \label{eq:metric:4d}
%   g_{\mu\nu} &=& \left(\begin{matrix}-\alpha^2+\beta_k\beta^k & \beta_i\\\beta_j&\gamma_{ij}\end{matrix}\right)\\
%   \label{eq:metric:4d}
%   g^{\mu\nu} &=& \left(\begin{matrix}-1/\alpha^2 & \beta^/\alpha^2\\\beta^j/\alpha^2 & \gamma^{ij}-\beta^i\beta^j/\alpha^2\end{matrix}\right).
% \end{eqnarray}
