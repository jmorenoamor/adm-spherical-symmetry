#!/usr/bin/env python2

"""Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-03-29 19:03:00 (jmiller)>

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

This module defines the right-hand-side of the constraint equations,
which solve for the gravitational quantities as a function of radius.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np # numpy arrays
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------

def get_a_prime(r,a,krr,rho,jr,s):
    """
    Calculates the radial derivative of the metric quantity a.

    Parameters
    ----------
    -- r, the radius
    -- a, the square root of the (r,r)-component of the metric.
    -- krr, extrinsic curvature (with one index up)
    -- rho, the matter energy density
    -- jr, the radial component of the matter current (index up)
    -- s, the trace of the stress tensor.
    """
    # special case to regularize system and impose boundary conditions
    if type(r) == float:
        if r == 0:
            return 0
        else:
            return (a/(2.*r))*((r**2) * (16*np.pi*rho
                                         - (3.0/2.0)*(krr**2)) -a + 1)
    else:
        out = np.zeros_like(r)
        out[1:] = (a[1:]/(2.*r[1:]))*((r[1:]**2) * (16*np.pi*rho[1:]
                                                    - (3.0/2.0)*(krr[1:]**2))
                                      -a[1:] + np.ones_like(r[1:]))
        return out


def get_krr_prime(r,a,krr,rho,jr,s):
    """
    Calculates the radial derivative of the (r,r)-component of the extrinsic
    curvature (with one index up)

    Parameters
    ----------
    -- r, the radius
    -- a, the square root of the (r,r)-component of the metric.
    -- krr, extrinsic curvature (with one index up)
    -- rho, the matter energy density
    -- jr, the radial component of the matter current (index up)
    -- s, the trace of the stress tensor.
    """
    # special case to regularize system and impose boundary conditions
    if r == 0: 
        return 0
    return 8*np.pi*(a**2)*jr - (3.0/r)*krr


def get_alphat_prime(r,a,krr,rho,jr,s,alphat,aleph):
    """
    Calculates the radial derivative of the lapse.

    Parameters
    ----------
    -- r, the radius
    -- a, the square root of the (r,r)-component of the metric.
    -- krr, extrinsic curvature (with one index up)
    -- rho, the matter energy density
    -- jr, the radial component of the matter current (index up)
    -- s, the trace of the stress tensor.
    -- alpha, the lapse
    -- aleph, canonically conjugate to the lapse
    """
    if r == 0: # boundary condition at origin
        return 0
    else:
        return aleph


def get_aleph_prime(r,a,krr,rho,jr,s,alphat,aleph):
    """
    Calculates the radial derivative of aleph, which is canonically
    conjugate to the lapse.

    Parameters
    ----------
    -- r, the radius
    -- a, the square root of the (r,r)-component of the metric.
    -- krr, extrinsic curvature (with one index up)
    -- rho, the matter energy density
    -- jr, the radial component of the matter current (index up)
    -- s, the trace of the stress tensor.
    -- alpha, the lapse
    -- aleph, canonically conjugate to the lapse
    """
    if r == 0:
        out = 0
    else:
        aprime = get_a_prime(r,a,krr,rho,jr,s)
        out = ((np.exp(-alphat) + (aprime/a)-(2./r))*aleph
               +(a**2)*((3./2.)*(krr**2)+4*np.pi*(rho+s)))
    return out


def get_hypersurface_prime(r,hypersurface_vector,matter_func):
    """
    Returns the right-hand-side of the relevant Einstein constraint equations
    for the gravity sector.

    Parameters
    ----------
    -- r, the radius
    
    -- hypersurface_vector, array-like. Contains metric quantity a,
       curvature quantity krr, lapse alpha, and dlapse/dr aleph:
       np.array([a,krr,alpha,aleph])
       -- a, the square root of the (r,r)-component of the metric.
       -- krr, extrinsic curvature (with one index up)
       -- alpha, the lapse
       -- aleph, the derivative of the lapse with respect to r.
    
    -- matter_func, array-valued function of r. 
       Should return the relevant parts of the energy-momentum
       tensor:
       r -> np.array([rho,jr,S])
       -- rho, the matter energy density
       -- jr, the radial component of the matter current (index up)
       -- S, the trace of the stress tensor.

    """
    # return matter quantities
    matter_vector = matter_func(r)
    # convenient names
    a = hypersurface_vector[A_I]
    krr = hypersurface_vector[K_I]
    rho = matter_vector[RHO_I]
    jr = matter_vector[J_I]
    s = matter_vector[S_I]
    out = np.empty_like(hypersurface_vector)
    out[A_I] = get_a_prime(r,a,krr,rho,jr,s)
    out[K_I] = get_krr_prime(r,a,krr,rho,jr,s)
    return out

# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
