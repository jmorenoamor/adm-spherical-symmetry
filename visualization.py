#!/usr/bin/env python2

"""
Time-stamp: <2015-03-29 22:29:30 (jmiller)>
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)

This module is part of the adm_spherical_symmetry package, which
solves the ADM equations for gravity in spherical symmetry.

This module defines numerical plotting
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams.update({'font.size': 22})

import data_structures
from data_structures import A_I,K_I,RHO_I,J_I,S_I
# ----------------------------------------------------------------------

# Constants
# ----------------------------------------------------------------------
PLOT_NAME="Gravity in Spherical Symmetry"
XLABEL="radius"
NX = 200
LINEWIDTH=5
# ----------------------------------------------------------------------

def my_plot(x,y,var_name):
    """
    generates a plot of x and y
    """
    plt.plot(x,y,linewidth=LINEWIDTH)
    plt.xlabel('r')
    plt.title(PLOT_NAME)
    plt.ylabel(var_name)
    plt.show()

def plot_hypersurface(stencil,hypersurface_variables,alpha,beta,matter_variables):
    """
    Generates a plot.
    """
    hypersurface_continuum = [None for i in range(hypersurface_variables.shape[1])]
    for i in range(len(hypersurface_continuum)):
        hypersurface_continuum[i] = stencil.to_continuum(hypersurface_variables[...,i])
    lin_x = np.linspace(stencil.xmin,stencil.xmax,NX)
    matter_vector = np.array([matter_variables(x) for x in lin_x])
    for i in range(matter_vector.shape[1]):
        my_plot(lin_x,matter_vector[...,i],data_structures.MATTER_NAMES[i])
    for i,var in enumerate(hypersurface_continuum):
        lin_x,lin_y = var.linspace(NX)
        my_plot(lin_x,lin_y,data_structures.HYPERSURFACE_NAMES[i])
    alphac = stencil.to_continuum(alpha)
    betac = stencil.to_continuum(beta)
    lin_x,lin_y = alphac.linspace(NX)
    my_plot(lin_x,lin_y,r'$\alpha$')
    lin_x,lin_y = betac.linspace(NX)
    my_plot(lin_x,lin_y,r'$\beta$')

#     lines = [plt.plot(rcoords,hypersurface_variables[...,i]) \
#              for i in range(hypersurface_variables.shape[-1])]
#     lines += [plt.plot(rcoords,shift)]
#     lines += [plt.plot(rcoords,matter_variables[...,i])\
#               for i in range(matter_variables.shape[-1])]
#     plt.xlabel(XLABEL)
#     plt.title(PLOT_NAME)
#     plt.legend(PLOT_LEGEND,
#                loc='center left',
#                bbox_to_anchor=(1.0,0.5),
#                fancybox=True,
#                shadow=True,
#                ncol=1)
#     plt.savefig(OUTFILE_NAME,bbox_inches='tight')
#     plt.show()
#    plt.plot(rcoords,hypersurface_variables[...,-2])
#    plt.show()
#    plt.plot(rcoords,hypersurface_variables[...,-1])
#    plt.show()
    return

# Warning not to run this program on the command line
if __name__ == "__main__":
    raise ImportError("Warning. This is a library. It contains no main function.")
